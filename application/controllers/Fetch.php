<!-- chandra@misteraladin.com -->
<?php
defined('BASEPATH') OR exit('No direct script allowed');

/*----------------------------------------REQUIRE THIS PLUGIN----------------------------------------*/
require APPPATH . '/libraries/REST_Controller.php';
//use Restserver\Libraries\REST_Controller;

class Fetch extends REST_Controller{
	/*----------------------------------------CONSTRUCTOR----------------------------------------*/
	var $api = "";
	function __construct($config = 'rest'){
		parent::__construct($config);
		$this->load->database();
		$this->api = "https://randomfox.ca/floof/";
		$this->load->library('session');
		$this->load->library('curl');
		$this->load->helper('url');
	}

	function index_get(){
		$dataImg = json_decode($this->curl->simple_get($this->api));
		$img = explode("/", $dataImg->image);
		$image = $img[count($img)-1];

		$link = "img/".$image;
		$url = $dataImg->image;
		$img = $link;
		file_put_contents($img, file_get_contents($url));

		$data = array(
			'image'	=>	$dataImg->image,
			'link'	=>	$link,
		);
		$insert = $this->db->insert('image', $data);
		
		if($insert){
			$this->response($data, 200);
		}
		else{
			$this->response(array('status' => 'fail', 502));
		}
	}


}
?>
