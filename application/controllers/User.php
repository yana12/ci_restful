<?php
defined('BASEPATH') OR exit('No direct script allowed');

/*----------------------------------------REQUIRE THIS PLUGIN----------------------------------------*/
require APPPATH . '/libraries/REST_Controller.php';
//use Restserver\Libraries\REST_Controller;

class User extends REST_Controller{
	/*----------------------------------------CONSTRUCTOR----------------------------------------*/
	function __construct($config = 'rest'){
		parent::__construct($config);
		$this->load->database();
	}

	function index_get(){
		$id = $this->get('id');
		
		if($id == ''){
			$kontak = $this->db->get('user')->result();
		}
		else{
			$this->db->where('id', $id);
			$kontak = $this->db->get('user')->result();
		}

		$this->response($kontak, 200);
	}

	function index_post(){
		$name = $this->post('name');
		$email = $this->post('email');
		$phone = $this->post('phone_number');
		if ( !preg_match ("/^[a-zA-Z\s]+$/",$name)) {
			$this->response(array(
				'status' => 'fail',
				'message' => 'name only character alphabet', 
				502));
		} 
		if ( !filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$this->response(array(
				'status' => 'fail',
				'message' => 'email not valid format', 
				502));
		} 
		if ( !preg_match ("/^[0-9]+$/",$phone)) {
			$this->response(array(
				'status' => 'fail',
				'message' => 'phone_number only numeric', 
				502));
		} 
		$data = array(
			'id'	=>	$this->post('id'),
			'name'	=>	$name,
			'email'	=>	$email,
			'phone_number'	=>	$phone,
		);
		$insert = $this->db->insert('user', $data);
		
		if($insert){
			$this->response($data, 200);
		}
		else{
			$this->response(array('status' => 'fail', 502));
		}
	}

	function index_put(){
		$id = $this->put('id');
		$name = $this->put('name');
		$email = $this->put('email');
		$phone = $this->put('phone_number');
		if ( !preg_match ("/^[a-zA-Z\s]+$/",$name)) {
			$this->response(array(
				'status' => 'fail',
				'message' => 'name only character alphabet', 
				502));
		} 
		if ( !filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$this->response(array(
				'status' => 'fail',
				'message' => 'email not valid format', 
				502));
		} 
		if ( !preg_match ("/^[0-9]+$/",$phone)) {
			$this->response(array(
				'status' => 'fail',
				'message' => 'phone_number only numeric', 
				502));
		} 
		$data = array(
			'id'	=>	$id,
			'name'	=>	$name,
			'email'	=>	$email,
			'phone_number'	=>	$phone,
		);
		
		$this->db->where('id', $id);
		$update = $this->db->update('user', $data);

		if($update){
			$this->response($data, 200);
		}
		else{
			$this->response(array('status' => 'fail'), 502);
		}
	}

	function index_delete(){
		$id = $this->delete('id');

		$this->db->where('id', $id);
		
		$delete = $this->db->delete('user');

		if($delete){
			$this->response(array('status' => 'success'), 201);
		}
		else{
			$this->response(array('status' => 'fail'), 502);
		}
	}
}
?>