User API with CI & MySQL

base_url = http://localhost/ci_restful/index.php/

Resource Endpoint

1. Create User

method : POST

endpoint : /user

parameters:
name : String
email : String
phone_number : Number


2. List User

method : GET

endpoint : /user

3. List User with ID

method : GET

endpoint : /user

parameters:
id : Number


4. Update User

method : PUT

endpoint : /user

parameters:
id : Number
name : String
email : String
phone_number : Number


5. Delete User

method : DELETE

endpoint : /user

parameters:
id : Number

